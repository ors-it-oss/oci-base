#!/bin/bash
gem_install(){
	header "Installing gem ${@}..."
	gem install --no-update-sources --no-document "$@"
}
