#!/bin/bash
pip_install(){
	header "Installing $*..."
  if [[ -x /usr/bin/pip3 ]];then
    pypip=/usr/bin/pip3
  elif [[ -x /usr/bin/pip2 ]]; then
    pypip=/usr/bin/pip2
  else
    pypip=pip
  fi
  $pypip --no-cache-dir install "$@"
}

py_compile() {
  # Python usually does this on the fly
  #   good luck w/that on a read-only mounted image
  python -m compileall
}
