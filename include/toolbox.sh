#!/bin/bash


####---------------- Toolbox containers ----------------####
# Run an interactive container with user's $HOME and env integrated
toolbox(){
	[[ -n "$DEBUG" ]] && set -x
	name="${name}_$(date +%s)"

	if [[ "$toolbox_target" == "k8s" ]]; then
		kube_run "$@"
		return $?
	fi
	
	runas=${runas:-$UID:$GUID}
	# Prepare a passwd & group file for those pesky daemons who won't start if $UID/$GUID is not in there
	[[ -f /run/user/${UID}/${UID}.passwd ]] || getent passwd $UID >/run/user/${UID}/${UID}.passwd
	[[ -f /run/user/${UID}/${GUID}.group ]] || getent group $GUID >/run/user/${UID}/${GUID}.group

	# SELinux
	# https://danwalsh.livejournal.com/74754.html
	# We might switch to --security-opt label=type:unconfined_t \ or sumtin' even better (how 'bout we create our own?) 
	# This label=disable makes the container SElinux 'Super Privileged Container' type (spc_t)
	# 
	# Satellite: Pass on /usr/share/rhel/secrets symlinks the way Buildah/Podman do it by default
#		--env HOME=$HOME \
	podman run --name $name --rm -ti \
		--hostname $fqdn --net host \
		--user "$runas" --security-opt label=disable --security-opt seccomp=/usr/share/containers/seccomp.json \
		--workdir /src \
		--tmpfs /run:noexec --tmpfs /tmp:noexec \
		${SSH_AUTH_SOCK:+--env SSH_AUTH_SOCK=${SSH_AUTH_SOCK}} \
		${SSH_AUTH_SOCK:+--volume $(readlink -f ${SSH_AUTH_SOCK}):${SSH_AUTH_SOCK}:z} \
		--volume /run/user/${UID}/${UID}.passwd:/etc/passwd:z \
		--volume /run/user/${UID}/${GUID}.group:/etc/group:z \
		--env TERM=xterm-256color \
		${HTTP_PROXY:+--env HTTP_PROXY="$HTTP_PROXY"} \
		${HTTPS_PROXY:+--env HTTPS_PROXY="$HTTPS_PROXY"} \
		${NO_PROXY:+--env NO_PROXY="$NO_PROXY"} \
		${http_proxy:+--env http_proxy="$http_proxy"} \
		${https_proxy:+--env https_proxy="$https_proxy"} \
		${no_proxy:+--env no_proxy="$no_proxy"} \
		--env-file <(for k in $envs; do echo "$k=${!k}"; done) \
		--volume /etc/pki/tls/cert.pem:/etc/pki/tls/cert.pem:ro \
		--volume /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem:/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem:ro \
		--volume /etc/pki/tls/certs/ca-bundle.crt:/etc/pki/tls/certs/ca-bundle.crt:ro \
		--volume $PWD:/src \
		--volume $HOME:$HOME \
		$volumes \
	$img "$@"
}


# Captain Ahab contains tons of useful container/k8s/helm mgmt stuffs
ahab(){
	img=quay.io/theloeki/toolbox:ahab
	name=Captain_Ahab
	fqdn=capt-ahab
	envs=KUBECONFIG
	toolbox "$@"
}

# Dr. HALO is useful for sysadmin-kinds of debugging stuffs
drhalo(){
	img=quay.io/theloeki/toolbox:drhalo
	name=Dr_HALO
	fqdn=dr-halo
	toolbox "$@"
}

# EIS is useful for eisy pplz
eis(){
	img=quay.io/theloeki/toolbox:eis
	name=EISbox
	fqdn=eisbox
	toolbox "$@"
}

# Fedora base
fedora(){
	img=toolbox-fedora:test1
	name=Fedora_toolbox
	fqdn=fedora-tb
	toolbox "$@"
}


####---------------- Kubernetes ----------------####
# Run the Kubernetes dashboard locally against a user's kubeconfig
kube_dash(){
	kubeconfig="${KUBECONFIG:-$HOME/.kube/config}"
	ns="${NAMESPACE:-$(kubectl config view -o jsonpath='{.contexts[].context.namespace}')}"
	ns="${ns:-kube-system}"

	docker run --name k8sBUI --rm \
		--user ${UID}:${GUID} \
		--read-only --tmpfs /run:exec --tmpfs /tmp:exec \
		--volume ${kubeconfig}:${kubeconfig}:ro \
		--net host \
	docker.io/kubernetesui/dashboard:v2.0.4 \
		--logtostderr \
		--port 9090 \
		--kubeconfig "$kubeconfig" \
		--namespace "$ns"
}

#kube_exec(){
#	pod=$1
#	shift
#	kubectl exec -ti "$@"
#}

# Run an interactive pod within Kubernetes
kube_run(){
	[[ -n "$DEBUG" ]] && set -x
	kubectl run --rm -ti $fqdn \
		--generator=run-pod/v1 --restart=Never \
		--env TERM=xterm-256color \
		--env HTTP_PROXY="$HTTP_PROXY" \
		--env HTTPS_PROXY="$HTTPS_PROXY" \
		--env NO_PROXY="$NO_PROXY" \
		--env http_proxy="$http_proxy" \
		--env https_proxy="$https_proxy" \
		--env no_proxy="$no_proxy" \
	--image=$img "$@"
}

