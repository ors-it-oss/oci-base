#!/bin/bash

set -a
os_major=$(rpm -E '%{rhel}')
pkgr="yum -y --setopt=tsflags=nodocs"
pkgr_rhbase=(
  "rhel-$os_major-server-rpms"
  "rhel-$os_major-server-rh-common-rpms"
  "rhel-$os_major-server-extras-rpms"
  "rhel-$os_major-server-supplementary-rpms"
  "rhel-$os_major-server-optional-rpms"
)
set +a

repo_enable() {
  yum-config-manager $(for repo in "$@"; do echo -n "--enable $repo "; done)
}
