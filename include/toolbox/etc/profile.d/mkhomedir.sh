#@IgnoreInspection BashAddShebang
# shellcheck shell=bash

mkhome=${HOME:-$(getent passwd $UID|cut -d: -f6)}

if [[ -z "$mkhome" ]]; then
  echo "WARNING NO HOME"
elif [[ ! -d "$mkhome" ]]; then
  mkdir -pm 750 "$mkhome"
  cp -rd --preserve=mode,timestamps /etc/skel/. "$mkhome"
fi

unset mkhome
