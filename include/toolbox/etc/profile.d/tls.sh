####---------------- TLS ----------------####
tls_certs(){
  case ${1##*:} in
    389) proto=ldap ;;
    5432) proto=postgres ;;
    25) proto=smtp ;;
    *) proto= ;;
  esac
  openssl s_client -connect "$1" -showcerts ${proto:+-starttls $proto} </dev/null | awk '/^-----BEGIN CERT/,/^-----END CERT/'
}

tls_info(){
  if [[ -f "$1" ]]; then
    openssl crl2pkcs7 -nocrl -certfile "$1" | openssl pkcs7 -print_certs -text -noout
  else
    openssl crl2pkcs7 -nocrl -certfile <(tls_certs "$1") | openssl pkcs7 -print_certs -text -noout
  fi
}
