#!/bin/bash


####---------------- Git ----------------####
gac(){
	git add --all :/
	git commit -v && git push
}

gmm(){
	target=${1:-master}
  branch=$(git rev-parse --abbrev-ref HEAD)
  [[ "$branch" != "$target" ]] || return 1
  git checkout "$target"
  git merge --squash -X theirs $branch
  git commit -v &&
  git push
  git checkout "$branch"
}

wtc(){
  curl -sSL http://whatthecommit.com/index.txt
}
