#!/bin/bash
golangci_install(){
	[[ "$GOLANGCI_VERSION" == "latest" ]] && unset GOLANGCI_VERSION
	fetch https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin ${GOLANGCI_VERSION:+v$GOLANGCI_VERSION}
	[[ -d "/etc/bash_completion.d/" ]] && golangci-lint completion bash >/etc/bash_completion.d/golangci-lint
	golangci-lint --version
}

upx_install(){
	eval $(github_uurlb upx/upx "$UPX_VERSION") \
	upst_bin="upx"; upst_blob="upx-${upst_version}-${GOARCH}_linux.tar.xz" \
	blob_binstall
	upx -V
}
