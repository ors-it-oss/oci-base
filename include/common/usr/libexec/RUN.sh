#!/bin/bash
# TODO: Weird friggin' shit: doing a [1,2,3]|join(".") works from cli, but not from Dockerfile:
# jq: error (at <stdin>:0): string ("") and number (1) cannot be added
# but seems only in hashi_install not in github_release wtf
# & map_values(tostring) fixes it
#

#------------------------ Global environment ------------------------#
if [[ -n "$MEDEVEL" ]]; then
  set -x
else
  set -eo pipefail
  [[ -n "$DEBUG_CURL" ]] && fetch_verbose=Ssv
fi

if [[ -z "$ARCH" ]]; then
  ARCH=$(uname -m)
fi

if [[ -z "$GOARCH" ]]; then
	case $ARCH in
		x86_64) export GOARCH=amd64 ;;
		aarch64) export GOARCH=aarch64 ;;
		*) echo "UNKNOWN ARCHITECTURE"; return 1 ;;
esac; fi

[[ -f /etc/os-release ]] && . /etc/os-release

#------------------------ Common tools ------------------------#
header() {
  # Nicely formatted demarkation line. You're welcome ^^
  cat >&2 <<-TPL


		########
		########---------------- $1 ----------------########
		########
		${2+######## $2}
		
TPL
}

fetch() {
  # Simple wrapper around curl
  # USAGE:
  #   fetch https://example.com/one
  #   fetch -o /tmp/example.yml https://example.com/one
  curl -fL${fetch_verbose} "$@"
}

section_start(){
	section="$1";	shift
	echo -e "\e[0Ksection_start:`date +%s`:$section\r\e[0K$@"
	header "$@"
}

section_end(){
	echo -e "\e[0Ksection_end:`date +%s`:$1\r\e[0K"
}


#------------------------ Installers&Extractors ------------------------#
blob_bextract() {
  # Extract an archive $upst_blob to an executable target $upst_bin
	target_bin="$xD/$(basename "${upst_bin:?}")"
	case "${upst_blob:?}" in
		*\.tar\.gz|*\.tgz) local fmt=tar; local fmt_flags=z ;;
		*\.tar\.xz|*\.txz) local fmt=tar; local fmt_flags=J ;;
		*\.gz) local fmt=gzip ;;
		*\.zip) local fmt=zip ;;
		*) local fmt=bin ;;
	esac

	case $fmt in
		bin) mv "$upst_blob" "$target_bin" ;;
		gzip) mv "$upst_blob" "$xD"; gunzip -vN "$xD/$upst_blob"; mv "$xD/${upst_blob%.gz}" "$target_bin" ;;
		tar)
			# I don't trust tar's glob matching enough for this
			local arch_bin="$(tar -t${fmt_flags}f "$upst_blob" | grep -E "(.*/)?${upst_bin}\$")"
			tar -xvo${fmt_flags}f "$upst_blob" -C "$xD" --xform='s|.*/||' "$arch_bin"
		;;
		zip)
			local arch_bin="$(unzip -l "$upst_blob" | grep -E "(.*/)?${upst_bin}\$")"
			unzip -jod "$xD" "$upst_blob" "$upst_bin"
		;;
		*) return 0 ;;
	esac

	chmod +x "$target_bin"

}

blob_binstall() {
	header "Installing ${upst_bin} ${upst_version}..."
	local tmpD=$(mktemp -d); cd $tmpD
	xD="${xD:-/bin}"
	for a in $upst_blob $upst_kmf $upst_sig $upst_sumf; do
		fetch -O "$upst_urlb/$a"
	done
	[[ -n "$upst_sum$upst_sumf" ]] && { blob_validate || return 1; }

	blob_bextract

	if [[ -z "$keepTmp" ]]; then
		cd ..; rm -rf "$tmpD"
	fi
}

blob_validate() {
	echo -n "Validating checksum of ${upst_blob:?}:"
	if [[ -f "$upst_sumf" ]]; then
		upst_sum="$(grep -E "[ /]${upst_blob}\$" "$upst_sumf")"
		upst_sum="${upst_sum:-$(cat "$upst_sumf")}"
	fi
	upst_sum="${upst_sum%% *}"

	case ${#upst_sum} in
		32) summer=md5sum ;;
		40) summer=sha1sum ;;
		56)	summer=sha3sum ;;
		64) summer=sha256sum ;;
		96) summer=sha384sum ;;
		128) summer=sha512sum ;;
    *) echo 'UNKNOWN CHECKSUM TYPE'; return 1 ;;
	esac

	if [[ "$($summer "$upst_blob")" == "$upst_sum  $upst_blob" ]]; then
		echo 'OK'
	else
		echo 'BAD CHECKSUM'; return 1
	fi
}

pkg_run() {
  # Perform updates first if asked, install packages and cleanup afterwards
  if [[ -n "$update_all" ]]; then
    header "Updating packages..."
    $pkgr update
  fi
  header "Installing packages..."
  $pkgr install "$@"
  rm -rf /var/cache/{dnf,yum}/* /var/log/{dnf,yum}*
}

#------------------------ Include all others ------------------------#
for incl in /usr/libexec/RUN.d/*.sh; do
	case $incl in
		dnf.sh) [[ -x /usr/bin/dnf || -x /usr/bin/microdnf ]] && . $incl ;;
		yum.sh) [[ -x /usr/bin/yum ]] && . $incl ;;
		*) . $incl ;;
	esac
done

if [[ -n "$@" ]]; then
	"$@"
fi
