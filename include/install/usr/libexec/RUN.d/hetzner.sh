#!/bin/bash
hcloud_install(){
	eval $(github_uurlb hetznercloud/cli "$HCLOUD_VERSION")
	upst_bin=hcloud
	upst_blob="hcloud-linux-${GOARCH}.tar.gz"
	upst_sumf=checksums.txt
	blob_binstall

	hcloud completion bash >/etc/bash_completion.d/hcloud

	hcloud version
}
