#!/bin/sh
dive_install(){
	eval $(github_uurlb wagoodman/dive "$DIVE_VERSION")
	upst_bin=dive
	upst_blob="dive_${upst_version}_linux_${GOARCH}.tar.gz"
  upst_sumf="dive_${upst_version}_checksums.txt"

	blob_binstall

	dive -v
}

helmfile_install(){
	eval $(github_uurlb roboll/helmfile "$HELMFILE_VERSION")
	upst_bin=helmfile
	upst_blob="helmfile_linux_${GOARCH}"
	blob_binstall

	helmfile -v
}

k9s_install(){
	if [[ "$ARCH" == "aarch64" ]]; then
		upst_arch=arm64
	else
		upst_arch="$ARCH"
	fi
	eval $(github_uurlb derailed/k9s "$K9S_VERSION")
	upst_bin=k9s
	upst_blob="k9s_Linux_${upst_arch}.tar.gz"
	upst_sumf="checksums.txt"
	blob_binstall

	k9s version
}

kboom_install(){
	upst_bin=kubectl-boom
	upst_blob=kboom
	upst_urlb=https://raw.githubusercontent.com/mhausenblas/kboom/master
	# /bin is symlinked to /usr/bin, so putting plugins in there will make kubectl go bonkers on overshadowing
	xD=/usr/local/bin
	blob_binstall
}
