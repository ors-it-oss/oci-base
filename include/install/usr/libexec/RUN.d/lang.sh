#!/bin/bash
shfmt_install(){
	header "Installing shfmt..."

  local jqscript="$(cat <<-'JQSCRIPT'
	{
	  upst_version: .tag_name|sub("^v"; ""),
	  upst_url: .assets|map(select(.name|contains("linux_" + $ARCH)))[]|.browser_download_url
	} | keys[] as $k | "local \($k)=\(.[$k])"
	JQSCRIPT
  )"

  eval $(github_release mvdan/sh $SHFMT_VERSION | jq -r --arg ARCH "$GOARCH" "$jqscript")
	fetch -o /bin/shfmt "$upst_url"
	chmod +x /bin/shfmt
	shfmt -version

}
