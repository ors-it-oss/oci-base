#!/bin/bash
azure_install() {
	pip_install azure-cli
}

azure_repo() {
	rpm --import https://packages.microsoft.com/keys/microsoft.asc
	cat >/etc/yum.repos.d/azure-cli.repo <<-REPO
		[azure-cli]
		name=Azure CLI
		baseurl=https://packages.microsoft.com/yumrepos/azure-cli
		enabled=1
		gpgcheck=1
		gpgkey=https://packages.microsoft.com/keys/microsoft.asc
	REPO
}
