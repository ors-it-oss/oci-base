#!/bin/bash
fcct_install(){
	eval $(github_uurlb coreos/fcct "$FCCT_VERSION")
	upst_bin=fcct
	upst_blob="fcct-${ARCH}-unknown-linux-gnu"
	blob_binstall

	fcct --version
}

satellite_cli_repo(){
	header "Enabling Foreman and Katello repos..."
	rpm -Uhv \
		https://yum.theforeman.org/releases/$FOREMAN_VERSION/el8/$ARCH/foreman-release.rpm \
		https://fedorapeople.org/groups/katello/releases/yum/$KATELLO_VERSION/katello/el8/$ARCH/katello-repos-latest.rpm
	sed -i '/\[pulp\]/,/^ *\[/ s/enabled=1/enabled=0/' /etc/yum.repos.d/katello.repo
}
