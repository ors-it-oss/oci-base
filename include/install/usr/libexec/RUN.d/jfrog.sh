#!/bin/bash
jfrog_install(){
	cd /bin
	header "Installing JFrog CLI..."
	fetch https://getcli.jfrog.io | sh
	chmod +x /bin/jfrog
	HOME=/tmp jfrog completion bash && install -m 644 /tmp/.jfrog/jfrog_bash_completion /etc/bash_completion.d/jfrog && rm -rf /tmp/.jfrog
	jfrog --version
}
