#!/bin/bash
govc_install(){
	if [[ "$ARCH" == "aarch64" ]]; then
		upst_arch=arm64
	else
		upst_arch="$ARCH"
	fi
	eval $(github_uurlb vmware/govmomi "$GOVC_VERSION")
	upst_bin=govc
	upst_blob="govc_Linux_${upst_arch}.tar.gz"
	upst_sumf="checksums.txt"
	blob_binstall

	fetch https://raw.githubusercontent.com/vmware/govmomi/master/scripts/govc_bash_completion >/etc/bash_completion.d/govc

	govc version
}

sonobuoy_install(){
	eval $(github_uurlb vmware-tanzu/sonobuoy "$SONOBUOY_VERSION")
	upst_bin=sonobuoy
	upst_blob="sonobuoy_${upst_version}_linux_${GOARCH}.tar.gz"
	upst_sumf="sonobuoy_${upst_version}_checksums.txt"
	blob_binstall

	sonobuoy version	
}

vsphere_pysdk_install(){
	eval $(github_uurlb vmware/vsphere-automation-sdk-python "$VSPHERE_PYSDK_VERSION")
	tmpDir=$(mktemp -d)
	fetch https://github.com/vmware/vsphere-automation-sdk-python/archive/refs/tags/v${upst_version}.tar.gz | tar xzv --strip-components=1 -C "$tmpDir" -f -

	(cd "$tmpDir"; pip_install --extra-index-url file://$tmpDir/lib -r requirements.txt)

	rm -rf "$tmpDir"
}

velero_install(){
	eval $(github_uurlb vmware-tanzu/velero "$VELERO_VERSION")
	upst_bin=velero
	upst_blob="velero-v${upst_version}-linux-${GOARCH}.tar.gz"
	upst_sumf="CHECKSUM"
	blob_binstall

	velero completion bash >/etc/bash_completion.d/velero
	velero version --client-only
}

