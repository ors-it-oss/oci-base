#!/bin/bash
etcd_install(){
	eval $(github_uurlb etcd-io/etcd "$ETCD_VERSION")
	upst_bin=etcdctl
	upst_blob="etcd-v${upst_version}-linux-${GOARCH}.tar.gz"
	upst_sumf="SHA256SUMS"
	blob_binstall

	etcdctl version
}

helm_install(){
	eval $(github_uurlb helm/helm "$HELM_VERSION")
	upst_bin=helm
	upst_blob=helm-v${upst_version}-linux-${GOARCH}.tar.gz
	upst_sumf=${upst_blob}.sha256sum
	upst_urlb=https://get.helm.sh
	blob_binstall

	XDG_CACHE_HOME=/var/cache XDG_DATA_HOME=/var helm version

	#	echo 'Adding all Helm repos from Helm Hub as defaults...' ;\
	#	mkdir -pm 755 /etc/helm &&\
	#	curl -fLsSv -o /etc/helm/repositories.yaml &&\
	#	ln -sf /home/.cache/helm/repository/ ${XDG_CACHE_HOME}/repository &&\
	#	curl -fLsSv -o /etc/helm/repositories.yaml https://raw.githubusercontent.com/helm/hub/master/repos.yaml &&\

	#RUN \
	#	echo Installing Helm plugins... ;\
	#	helm plugin install https://github.com/databus23/helm-diff &&\
	#	rm -rf /tmp/helm-diff* &&\
	#	helm plugin install https://github.com/aslafy-z/helm-git.git &&\
	#	helm plugin install https://github.com/chartmuseum/helm-push &&\
	#	helm plugin install --version=1.0.2 https://github.com/belitre/helm-push-artifactory-plugin &&\
	#	helm plugin install https://github.com/hypnoglow/helm-s3.git &&\
	#	helm plugin install https://github.com/jkroepke/helm-secrets &&\
	#	helm completion bash >/etc/bash_completion.d/helm
}

krew_install(){
	eval $(github_uurlb kubernetes-sigs/krew "$KREW_VERSION")
	upst_bin=krew-linux_$GOARCH
	upst_blob="krew-linux_${GOARCH}.tar.gz"
	upst_kmf="krew.yaml"
	upst_sumf="${upst_blob}.sha256"
	keepTmp=1 blob_binstall
	fetch -O $upst_urlb/krew.yaml

	/bin/krew-linux_$GOARCH install --archive="$upst_blob" --manifest=krew.yaml
	tmpD=$(pwd); cd ..; rm -rf /bin/krew-linux_$GOARCH "$tmpD"

	kubectl krew update
	kubectl krew version
}

krew_install_plugins(){
	header "Installing Kubernetes control Krew plugins..."
	for plugin in "$@"; do
		kubectl krew install $plugin
	done
}

kubectl_install(){
	header "Installing kubectl-aliases by Alp Balkan..."
	fetch -o /etc/profile.d/ahmetb_kubectl.sh https://raw.githubusercontent.com/ahmetb/kubectl-aliases/master/.kubectl_aliases

	eval $(github_uurlb kubernetes/kubernetes "$K8S_VERSION")
	upst_bin=kubectl
	upst_blob=kubectl
	upst_sumf="${upst_blob}.sha256"
	upst_urlb=https://dl.k8s.io/release/v${upst_version}/bin/linux/${GOARCH}/
	blob_binstall

	kubectl version --client=true -o yaml
}

svcat_install(){
	upst_bin=svcat
	upst_blob=svcat
	upst_version=${SVCAT_VERSION:-latest}
	upst_urlb=https://download.svcat.sh/cli/$upst_version/linux/$GOARCH
	blob_binstall

	# 0.3.0 b0rks if not set
	export KUBERNETES_MASTER=dummy
	svcat completion bash >/etc/bash_completion.d/svcat
	svcat version --client
}
