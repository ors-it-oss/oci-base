#!/bin/bash
rundeck_repo(){
	header "Enabling RunDeck CLI repo..."
	# rpm --import https://raw.githubusercontent.com/rundeck/packaging/main/pubring.gpg
	cat >/etc/yum.repos.d/rundeck.repo <<-'RDREPO'
		[pagerduty_rundeck]
		name=pagerduty_rundeck
		baseurl=https://packagecloud.io/pagerduty/rundeck/rpm_any/rpm_any/$basearch
		repo_gpgcheck=0
		gpgcheck=0
		enabled=1
		gpgkey=https://packagecloud.io/pagerduty/rundeck/gpgkey
		sslverify=1
		sslcacert=/etc/pki/tls/certs/ca-bundle.crt
		metadata_expire=300
	RDREPO
}
