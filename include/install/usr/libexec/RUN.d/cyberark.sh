#!/bin/bash
#Gem breaks on unf_ext compilation
#also activesupport < 6 but centos8 delivers 6.0
conjur_install(){
	#	gem install -b --minimal-deps --no-document --no-user-install --no-update-sources $(gem dep -r conjur-cli| sed -E -e '/^ /!d' -e 's|^[[:space:]]+||' -e 's| \(|:|' -e 's|[),].*||' -e 's| ||g')
	gem_install conjur-cli
	conjur shellinit >/etc/bash_completion.d/conjur
	conjur -v
}
