#!/bin/bash
gcloud_install(){
	header "Installing Google Cloud SDK..."
	fetch https://sdk.cloud.google.com | bash -s -- --install-dir=/opt --disable-prompts
	ln -s /opt/google-cloud-sdk/completion.bash.inc /etc/bash_completion.d/google-cloud-sdk
	gcloud config set disable_usage_reporting true
	gcloud version

	# cat ~/.config/gcloud/configurations/config_default
	#[core]
	#disable_usage_reporting = false
	# nice for skel dir but that's unused

}

jsonnet_install(){
	eval $(github_uurlb google/jsonnet "$JSONNET_VERSION")
	upst_bin=jsonnet
	upst_blob="jsonnet-bin-v${upst_version}-linux.tar.gz"
	blob_binstall

	jsonnet --version
}
