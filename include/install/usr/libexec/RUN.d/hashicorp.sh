#!/bin/bash
hashi_install(){
  product=$1
  upst_version=$2
	if [[ -z "$upst_version" || "$upst_version" == latest ]]; then
		local jqscript="$(cat <<-'JQSCRIPT'
			.versions|keys|map(
				 select(contains("-")|not)|split(".")|map(tonumber)
			)|max|map_values(tostring)|join(".")
		JQSCRIPT
		)"
		upst_version="$(fetch https://releases.hashicorp.com/${product}/index.json | jq -r --arg ARCH "$GOARCH" "$jqscript")"
	fi
  [[ -n "$upst_version" ]] || return 1
	upst_urlb="https://releases.hashicorp.com/$product/$upst_version"
	upst_sumf="${product}_${upst_version}_SHA256SUMS"
	upst_sig="${upst_sumf}.sig"
	upst_blob="${product}_${upst_version}_linux_${GOARCH}.zip"
	upst_bin="$product"
	blob_binstall

  # HashiCorp be a bit weird
  touch /root/.bashrc
  case $product in
    packer) $product -autocomplete-install ;;
    terraform) $product -install-autocomplete ;;
    vagrant) vagrant autocomplete install bash ;;
  esac
  mv /root/.bashrc /etc/bash_completion.d/$product

  $product version
}


terragrunt_install(){
	eval $(github_uurlb gruntwork-io/terragrunt "$TERRAGRUNT_VERSION")
	upst_bin=terragrunt
	upst_blob="terragrunt_linux_${GOARCH}"
	blob_binstall

	terragrunt --version
}
