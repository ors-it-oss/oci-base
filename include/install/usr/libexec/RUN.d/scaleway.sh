#!/bin/bash
scw_install(){
	eval $(github_uurlb scaleway/scaleway-cli "$SCW_VERSION")
	upst_bin=scw
	upst_blob="scaleway-cli_${upst_version}_linux_${ARCH}"
	upst_sumf=SHA256SUMS
	blob_binstall

	scw autocomplete script shell=bash >/etc/bash_completion.d/scw

	scw version
}
