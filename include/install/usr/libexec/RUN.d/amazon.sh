#!/bin/bash
aws_install(){
	header "Installing Amazon AWS CLI..." ;\
	upst_urlb="https://awscli.amazonaws.com"
	upst_blob="awscli-exe-linux-${ARCH}.zip"
	upst_sig="${upstream_blob}.sig"

	tmpD=$(mktemp -d); cd $tmpD
	fetch -O $upst_urlb/$upst_blob
	unzip -o "$upst_blob"
	aws/install --update
	# needs groff-base
	echo "complete -C $(which aws_completer) aws" > /etc/bash_completion.d/aws
	cd ..; rm -rf "$tmpD"

	aws --version
}

s3cmd_install(){
	pip_install s3cmd
}
