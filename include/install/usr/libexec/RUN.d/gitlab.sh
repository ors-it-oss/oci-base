#!/bin/bash

glab_install(){
	eval $(github_uurlb profclems/glab "$GLAB_VERSION")
	upst_bin=glab
	upst_blob="glab_${upst_version}_Linux_${ARCH}.tar.gz"
	blob_binstall

	glab version
	glab completion >/etc/bash_completion.d/glab
}

pygitlab_install(){
	pip_install python-gitlab
	gitlab --version
}
