#!/bin/bash
####---------------- Github ----------------####
github_release(){
  local project=$1
  local version=${2:-latest}

  local jqscript="$(cat <<-'JQSCRIPT'
		if ["latest","edge"]|index($version) then
			.|map(
				select($version == "edge" or .prerelease == false)
			)|max_by(
				.tag_name|sub("^v"; "")|split("[.+-]";"")|(.[0:3]|map(tonumber))+.[3:]
			)
		else
			.|map(select(.tag_name == "v\($version)"))[]
		end
	JQSCRIPT
  )"

  fetch "https://api.github.com/repos/$project/releases" | jq -r --arg version "$version" "$jqscript"
}

github_uurlb(){
	local jqscript="$(cat <<-'JQSCRIPT'
			{
			  upst_version: .tag_name|sub("^v"; ""),
			  upst_urlb: .assets[0]|.browser_download_url[0:.browser_download_url|rindex("/")],
			} | keys[] as $k | "local \($k)=\(.[$k])"
		JQSCRIPT
	)"

	github_release "$1" "$2" | jq -r "$jqscript"
}

