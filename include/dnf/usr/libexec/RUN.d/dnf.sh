#!/bin/bash
set -a
os_major=$(rpm -E '%{rhel}')
pkgr="dnf -y --nodocs"
pkgr_rhbase=(
  "rhel-$os_major-for-$ARCH-baseos-rpms"
  "rhel-$os_major-for-$ARCH-appstream-rpms"
  "rhel-$os_major-for-$ARCH-supplementary-rpms"
)
set +a

if [[ -x /usr/bin/microdnf ]]; then
  repo_enable(){
    header "Enabling DNF repository $@"
    for repo in "$@"; do
      sed -Ei "/\[$repo\]/,/^ *\[/ s/enabled([[:space:]]*)=.*$/enabled\1=\11/" /etc/yum.repos.d/*
    done
  }
else 
  repo_enable() {
    header "Enabling DNF repository $@"
    dnf config-manager $(for repo in "$@"; do echo -n "--set-enabled $repo "; done)
  }
fi
