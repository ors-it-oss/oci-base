#!/bin/bash
img=registry.gitlab.com/ors-it-oss/oci-base/fedora

launch(){
	name="basetst_$(date +%s)"
	podman run --rm -ti --name $name --userns=keep-id --read-only \
		--security-opt label=disable \
		--workdir /src \
		--tmpfs /run:noexec --tmpfs /tmp:noexec \
		${SSH_AUTH_SOCK:+--env SSH_AUTH_SOCK=/home/.ssh/auth.sock} \
		${SSH_AUTH_SOCK:+--volume $(readlink -f ${SSH_AUTH_SOCK}):/home/.ssh/auth.sock:z} \
		--env HOME=/home \
		--env TERM=xterm-256color \
		--volume $HOME/.ssh:/home/.ssh:ro \
		--volume $PWD:/src:z \
	$img "$@"
}

if [[ -n "$1" ]]; then
	cmd=$1; shift
	case $cmd in
		apply|plan) tf $cmd "$@" ;;
		*) $cmd "$@" ;;
	esac
fi
